# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields, sequence_ordered
from trytond.pyson import Eval, Bool
from trytond.pool import PoolMeta, Pool
from trytond.i18n import gettext
from trytond.exceptions import UserError
from trytond.transaction import Transaction

_STATES = {'readonly':  Bool(Eval('rams_ok'))}
_DEPENDS = ['rams_ok']
__all__ = ['Project', 'ChecklistItemsGroup', 'ChecklistItem', 'ProjectChecklistItem']


class Project(metaclass=PoolMeta):
    __name__ = 'work.project'

    checklist_0 = fields.Many2Many(
        'work.project-checklist.item',
        'project', 'item', 'Checklist',
        states={'invisible': ~Eval('state').in_(['planning', 'closed'])},
        order=[('item.sequence', 'ASC')])
    checklist = fields.One2Many('work.project-checklist.item', 'project',
        'Checklist',
        states={'invisible': ~Eval('state').in_(['executing'])},
        readonly=True,
        order=[('item.sequence', 'ASC')])
    checklist_state = fields.Function(fields.Selection([
        ('', ''),
        ('defined', 'Defined'),
        ('partially ok', 'Partially OK'),
        ('ok', 'OK'),
        ('ok RAMS', 'OK RAMS')
        ], 'Checklist State', readonly=True), 'get_checklist_state')

    @classmethod
    def close(cls, projects):
        for project in projects:
            if project.checklist_state != 'ok RAMS':
                raise UserError(gettext(
                    'electrans_work_project_checklist.safety_checklist_is_not_ok'))
        super(Project, cls).close(projects)

    def get_checklist_state(self, _):
        if not self.checklist:
            return ''
        elif all(item.rams_ok for item in self.checklist):
            return 'ok RAMS'
        elif all(item.engineering_ok for item in self.checklist):
            return 'ok'
        elif any(item.engineering_ok for item in self.checklist):
            return 'partially ok'
        else:
            return 'defined'


class ChecklistItemsGroup(sequence_ordered(), ModelSQL, ModelView):
    'Checklist Items Group'
    __name__ = 'checklist.item.group'

    name = fields.Char('Inspection point', required=True)
    items = fields.One2Many('checklist.item', 'group', 'Evidences')
    items_as_text = fields.Function(fields.Text('Evidences'),
        'get_items_as_text')

    def get_items_as_text(self, _):
        return '\n'.join([item.rec_name for item in self.items])


class ChecklistItem(sequence_ordered(), ModelSQL, ModelView):
    'Checklist Item'
    __name__ = 'checklist.item'

    name = fields.Char('Evidence', required=True)
    group = fields.Many2One('checklist.item.group', 'Inspection point',
        required=True, ondelete='CASCADE')
    number = fields.Function(fields.Char('#'),
        'get_number')

    def get_number(self, _):
        return str(self.group.sequence) + '.' + str(self.sequence)

    def get_rec_name(self, _):
        return self.number + ' ' + self.group.name + " / " + self.name

    @fields.depends('sequence')
    def on_change_name(self):
        # this on_change its just to set the first register sequence equal to 1
        # if the first one is filled, the rest will auto fill
        if not self.sequence:
            self.sequence = 1


class ProjectChecklistItem(ModelSQL, ModelView):
    'Project - Checklist Item'
    __name__ = 'work.project-checklist.item'

    project = fields.Many2One(
        'work.project', "Project",
        required=True,
        ondelete='CASCADE',
        states=_STATES,
        depends=_DEPENDS)
    item = fields.Many2One(
        'checklist.item',
        "Evidence",
        required=True,
        ondelete='CASCADE',
        readonly=True)
    engineering_ok = fields.Boolean("Engineering OK")
    documents = fields.One2Many(
        'ir.attachment', 'resource', "Documents",
        states=_STATES,
        depends=_DEPENDS)
    notes = fields.Text(
        "Notes",
        states=_STATES,
        depends=_DEPENDS)
    rams_ok = fields.Boolean(
        "RAMS OK")
    rams_notes = fields.Text(
        "RAMS Notes",
        states=_STATES)
    item_number = fields.Function(
        fields.Char("#"),
        'get_item_number')
    item_group = fields.Function(
        fields.Char("Inspection point"),
        'get_item_group')
    item_name = fields.Function(
        fields.Char("Evidence"),
        'get_item_name')
    item_rec_name = fields.Function(
        fields.Char("Evidence"),
        'get_item_rec_name')
    engineering_date = fields.Date(
        "Engineering Date",
        readonly=True)
    engineer = fields.Many2One(
        'res.user', "Engineer",
        readonly=True)
    rams_date = fields.Date(
        "RAMS Date",
        readonly=True)

    @classmethod
    def write(cls, *args):
        User = Pool().get('res.user')
        Date = Pool().get('ir.date')
        actions = iter(args)
        for checklist, values in zip(actions, actions):
            if values.get('notes') or values.get('documents'):
                user = User.search([('employee', '=', Transaction().context.get('employee', 0))])
                values['engineer'] = user[0].id if user else 0
                values['engineering_date'] = Date.today()
            if values.get('rams_ok'):
                values['rams_date'] = Date.today()
        super(ProjectChecklistItem, cls).write(*args)

    def get_item_number(self, _):
        return self.item.number

    def get_item_group(self, _):
        return self.item.group.name

    def get_item_name(self, _):
        return self.item.name

    def get_item_rec_name(self, _):
        return self.item.rec_name
