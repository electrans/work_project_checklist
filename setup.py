import io
import os
import re
from configparser import ConfigParser
from setuptools import setup, find_packages

name = 'electrans_work_project_checklist'

ELECTRANS_MODULES = [
    'electrans_work_project'
]
TRYTONSPAIN_MODULES = [
]
NANTIC_MODULES = [
]
TRYTONZZ_MODULES = [
]
ZIKZAKMEDIA_MODULES = [
]


def read(fname, slice=None):
    content = io.open(
        os.path.join(os.path.dirname(__file__), fname),
        'r', encoding='utf-8').read()
    if slice:
        content = '\n'.join(content.splitlines()[slice])
    return content


def get_require_version(name):
    if minor_version % 2:
        require = '%s >= %s.%s.dev0, < %s.%s'
    else:
        require = '%s >= %s.%s, < %s.%s'
    require %= (name, major_version, minor_version,
                major_version, minor_version + 1)
    return require


config = ConfigParser()
config.read_file(open(os.path.join(os.path.dirname(__file__), 'tryton.cfg')))
info = dict(config.items('tryton'))
for key in ('depends', 'extras_depend', 'xml'):
    if key in info:
        info[key] = info[key].strip().splitlines()
version = info.get('version', '0.0.1')
major_version, minor_version, _ = version.split('.', 2)
major_version = int(major_version)
minor_version = int(minor_version)

series = '%s.%s' % (major_version, minor_version)
if minor_version % 2:
    branch = 'default'
else:
    branch = series

requires = []
for dep in info.get('depends', []):
    if dep in NANTIC_MODULES:
        requires.append('nantic-%(dep)s @ https://github.com/NaN-tic/'
                        'trytond-%(dep)s/archive/refs/heads/%(branch)s.zip'
                        % {'branch': branch, 'dep': dep})
    elif dep in TRYTONSPAIN_MODULES:
        requires.append('trytonspain-%(dep)s @ https://github.com/NaN-tic/'
                        'trytond-%(dep)s/archive/refs/heads/%(branch)s.zip'
                        % {'branch': branch, 'dep': dep})
    elif dep in ZIKZAKMEDIA_MODULES:
        requires.append('zikzakmedia-%(dep)s @ https://github.com/NaN-tic/'
                        'trytond-%(dep)s/archive/refs/heads/%(branch)s.zip'
                        % {'branch': branch, 'dep': dep})
    elif dep in TRYTONZZ_MODULES:
        requires.append('trytonzz-%(dep)s @ https://github.com/NaN-tic/'
                        'trytond-%(dep)s/archive/refs/heads/%(branch)s.zip'
                        % {'branch': branch, 'dep': dep})
    elif dep in ELECTRANS_MODULES:
        if dep == 'project_expenses':
            requires.append('%(dep)s @ https://bitbucket.org/electrans/'
                            'trytond-%(dep)s/get/%(branch)s.zip'
                            % {'branch': branch, 'dep': dep})
        else:
            repo_name = dep.replace('electrans_', '')
            requires.append('%(dep)s @ https://bitbucket.org/electrans/'
                            '%(repo_name)s/get/%(branch)s.zip'
                            % {'branch': branch, 'dep': dep, 'repo_name': repo_name})
    elif not re.match(r'(ir|res)(\W|$)', dep):
        requires.append(get_require_version('trytond_%s' % dep))
requires.append(get_require_version('trytond'))

tests_require = [
    get_require_version('proteus'),
]

tests_require = []
dependency_links = []
if minor_version % 2:
    dependency_links.append('https://trydevpi.tryton.org/')

if minor_version % 2:
    # Add development index for testing with proteus
    dependency_links.append('https://trydevpi.tryton.org/')

setup(name=name,
      version=version,
      description='Tryton module for project work project',
      long_description=read('README'),
      author='Electrans',
      author_email='oqueralto@electrans.com',
      url='http://www.electrans.com/',
      download_url="https://bitbucket.org/electrans/trytond-%s" % name,
      package_dir={'trytond.modules.electrans_work_project_checklist': '.'},
      packages=(
              ['trytond.modules.%s' % name]
              + ['trytond.modules.electrans_work_project_checklist.%s' % p for p in find_packages()]
      ),
      package_data={
          'trytond.modules.electrans_work_project_checklist': (info.get('xml', [])
                                                     + ['tryton.cfg', 'view/*.xml', 'locale/*.po', '*.fodt',
                                                        'icons/*.svg', 'tests/*.rst']),
      },
      classifiers=[
          'Development Status :: 5 - Production/Stable',
          'Environment :: Plugins',
          'Framework :: Tryton',
          'Intended Audience :: Developers',
          'Intended Audience :: Financial and Insurance Industry',
          'Intended Audience :: Legal Industry',
      ],
      license='GPL-3',
      install_requires=requires,
      dependency_links=dependency_links,
      zip_safe=False,
      entry_points="""
        [trytond.modules]
        electrans_work_project_checklist = trytond.modules.electrans_work_project_checklist
        """,
      test_suite='tests',
      test_loader='trytond.test_loader:Loader',
      tests_require=tests_require,
      )
