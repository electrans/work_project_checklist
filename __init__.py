# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import checklist


def register():
    Pool.register(
        checklist.Project,
        checklist.ChecklistItem,
        checklist.ChecklistItemsGroup,
        checklist.ProjectChecklistItem,
        module='electrans_work_project_checklist', type_='model')
